CREATE DATABASE Mywebsite DEFAULT CHARACTER SET utf8;

USE Mywebsite;

CREATE TABLE

user(
id int primary key AUTO_INCREMENT,
login_id varchar(255) UNIQUE NOT NULL,
name varchar(255) NOT NULL,
birth_date date NOT NULL,
password varchar(255) NOT NULL,
create_date date NOT NULL,
update_date date NOT NULL
);

CREATE TABLE 
buy(
id int primary key AUTO_INCREMENT,
user_id int NOT NULL,
total_price int NOT NULL,
delivery_method_id int NOT NULL,
paymethod_id int NOT NULL,
buy_date date NOT NULL
);

CREATE TABLE
delivery_method(
id int primary key AUTO_INCREMENT,
name varchar(255) NOT NULL,
price int NOT NULL
);

CREATE TABLE
pay_method(
id int primary key AUTO_INCREMENT,
name varchar(255) NOT NULL,
price int NOT NULL
);

CREATE TABLE
buy_detail(
id int primary key AUTO_INCREMENT,
buy_id int NOT NULL,
item_id int NOT NULL
);

CREATE TABLE
item(
id int primary key AUTO_INCREMENT,
detail text,
name varchar(255) NOT NULL,
price int NOT NULL,
fire_name varchar(255),
category varchar(255) UNIQUE NOT NULL,
maker varchar(255) UNIQUE NOT NULL
);

INSERT INTO 
delivery_method(
name,
price
)
VALUES(
'通常配送',
0
),
(
'高速配送',
500
);

INSERT INTO
pay_method(
name,
price
)
VALUES(
'コンビニ払い',
100
),
(
'代金引換',
50
);

ALTER TABLE item DROP INDEX category;
ALTER TABLE item DROP INDEX maker;
ALTER TABLE item CHANGE COLUMN fire_name file_name varchar(255);

ALTER TABLE buy CHANGE COLUMN paymethod_id pay_method_id int;

