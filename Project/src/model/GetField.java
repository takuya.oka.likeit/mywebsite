package model;

import java.util.ArrayList;

import beans.ItemBeans;

public class GetField {

	public static int getTotalItemPrice(ArrayList<ItemBeans> price) {
		int total = 0;
		for (ItemBeans item : price) {
			total += item.getPrice();
		}
		return total;
	}
}
