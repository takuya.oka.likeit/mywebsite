package controller;

import java.io.IOException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.UserDao;

/**
 * Servlet implementation class UserDel
 */
@WebServlet("/UserDel")
public class UserDel extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserDel() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		UserBeans user = (UserBeans)session.getAttribute("userLogin");
		if(user == null) {
			session.removeAttribute("cartItem");
		    response.sendRedirect("Login");
		}

      if(user != null) {
		String id = request.getParameter("id");
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String createDate = request.getParameter("createDate");
		String updateDate = request.getParameter("updateDate");

		UserBeans userBeans = new UserBeans(loginId,name,birthDate,createDate,updateDate);
		request.setAttribute("userDel", userBeans);
        request.setAttribute("userDelId", id);

        RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userDel.jsp");
        dispatcher.forward(request, response);
	}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		UserBeans user = (UserBeans)session.getAttribute("userLogin");
		if(user == null) {
		response.sendRedirect("Login");
		}

		String id = request.getParameter("id");
		UserDao userDao = new UserDao();
        try {
			userDao.userDel(id);
		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			request.setAttribute("err",e.toString());
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/error.jsp");
			dispatcher.forward(request, response);
		}

        RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userDelFi.jsp");
        dispatcher.forward(request, response);
	}

}
