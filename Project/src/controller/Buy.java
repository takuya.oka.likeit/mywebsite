package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.DeliveryMethodBeans;
import beans.PayMethodBeans;
import beans.UserBeans;
import dao.DeliveryMethodDao;
import dao.PayMethodDao;

/**
 * Servlet implementation class Buy
 */
@WebServlet("/Buy")
public class Buy extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public Buy() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		UserBeans user = (UserBeans)session.getAttribute("userLogin");
		if(user == null) {
			session.removeAttribute("cartItem");
		    response.sendRedirect("Login");
		}

		if(user != null) {

            DeliveryMethodDao delivery = new DeliveryMethodDao();
            List<DeliveryMethodBeans> deliveryAll;
			try {
				deliveryAll = delivery.deliveryMethodAll();

            request.setAttribute("deliveryAll", deliveryAll);

            PayMethodDao pay = new PayMethodDao();
            List<PayMethodBeans> payAll = pay.payMethodAll();

            request.setAttribute("payAll",payAll);

			} catch (SQLException e) {
				// TODO 自動生成された catch ブロック
				e.printStackTrace();
				request.setAttribute("err",e.toString());
				RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/error.jsp");
				dispatcher.forward(request, response);
			}

			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/buyInput.jsp");
			dispatcher.forward(request, response);

		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
}
