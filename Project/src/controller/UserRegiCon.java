package controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.UserDao;

/**
 * Servlet implementation class UserRegiCon
 */
@WebServlet("/UserRegiCon")
public class UserRegiCon extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserRegiCon() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		UserBeans user = (UserBeans)session.getAttribute("userLogin");
		if(user == null) {
			session.removeAttribute("cartItem");
		    response.sendRedirect("Login");
		}

		if(user != null){
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");
		String password = request.getParameter("password");
		String passwordc = request.getParameter("passwordc");

		UserDao userDao = new UserDao();
		List<UserBeans> userAll;
		try {
			userAll = userDao.findUserAll(loginId);

		if(password.equals(passwordc) && userAll == null) {

		UserBeans userRegi = new UserBeans(loginId,name,birthDate,password);

		request.setAttribute("userRegi",userRegi);

		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userRegiCon.jsp");
		dispatcher.forward(request, response);
		}else if(!(password.equals(passwordc)) || userAll != null){
		request.setAttribute("errRegi", "入力内容が異なります。");
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userRegiInput.jsp");
		dispatcher.forward(request, response);
		}

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			request.setAttribute("err",e.toString());
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/error.jsp");
			dispatcher.forward(request, response);
		}
		}
		}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

        HttpSession session = request.getSession();
		UserBeans user = (UserBeans)session.getAttribute("userLogin");
		if(user == null) {
			session.removeAttribute("cartItem");
		    response.sendRedirect("Login");
		}

        if(user != null) {
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String password = request.getParameter("password");
		String birthDate = request.getParameter("birthDate");

		UserDao userDao = new UserDao();
		try {
			userDao.userRegi(loginId, name, birthDate, password);
		} catch (NoSuchAlgorithmException | SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			request.setAttribute("err",e.toString());
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/error.jsp");
			dispatcher.forward(request, response);
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userRegiFi.jsp");
		dispatcher.forward(request, response);
	}
	}
}
