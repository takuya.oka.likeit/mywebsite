package controller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.UserDao;

/**
 * Servlet implementation class UserInfoUpd
 */
@WebServlet("/UserInfoUpd")
public class UserInfoUpd extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserInfoUpd() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

        HttpSession session = request.getSession();
		UserBeans user = (UserBeans)session.getAttribute("userLogin");
		if(user == null) {
			session.removeAttribute("cartItem");
		    response.sendRedirect("Login");
		}

        if(user != null) {
        String loginId = request.getParameter("loginId");
        String name = request.getParameter("name");
        String birthDate = request.getParameter("birthDate");

        UserBeans userUpd = new UserBeans(loginId,name,birthDate);

        request.setAttribute("userUpd", userUpd);

		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userInfoUpd.jsp");
		dispatcher.forward(request, response);
        }
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");

        HttpSession session = request.getSession();
		UserBeans user = (UserBeans)session.getAttribute("userLogin");
		if(user == null) {
			session.removeAttribute("cartItem");
		    response.sendRedirect("Login");
		}

      if(user != null) {
        String loginId = request.getParameter("loginId");
        String password = request.getParameter("password");
        String passwordc = request.getParameter("passwordc");
        String name = request.getParameter("name");
        String birthDate = request.getParameter("birthDate");

      if(password.equals(passwordc)) {
        UserDao userDao = new UserDao();
        try {
			userDao.userUpd(loginId,password,name,birthDate);

		} catch (NoSuchAlgorithmException | SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			request.setAttribute("err",e.toString());
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/error.jsp");
			dispatcher.forward(request, response);
		}

        RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userInfoUpdFi.jsp");
        dispatcher.forward(request, response);
      }else {
        	request.setAttribute("errUpd","パスワードとパスワード（確認）が異なります。");

        	RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userInfoUpd.jsp");
        	dispatcher.forward(request, response);
        }
        }
	}
}
