package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemBeans;
import beans.UserBeans;
import dao.BuyDao;
import dao.BuyDetailDao;

/**
 * Servlet implementation class BuyFi
 */
@WebServlet("/BuyFi")
public class BuyFi extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public BuyFi() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		UserBeans user = (UserBeans)session.getAttribute("userLogin");
		if(user == null) {
		   session.removeAttribute("cartItem");
		   response.sendRedirect("Login");
		}

		if(user != null) {
			ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>) session.getAttribute("cartItem");

			String totalPriceStr = request.getParameter("totalPrice");

			int userId = user.getId();

			int totalPrice = Integer.parseInt(totalPriceStr);

            String deliveryIdStr = request.getParameter("DeliveryId");
            int deliveryId = Integer.parseInt(deliveryIdStr);

            String payIdStr = request.getParameter("PayId");
            int payId = Integer.parseInt(payIdStr);

            BuyDao buy = new BuyDao();
            int buyId;
			try {
				buyId = buy.buy(userId, totalPrice, deliveryId, payId);

        if(buyId != -1) {
            for(ItemBeans cartItem:cart) {
            BuyDetailDao buyDetail = new BuyDetailDao();
            buyDetail.insertBuyDetail(buyId,cartItem.getId());
            }
        }

    	}catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			request.setAttribute("err",e.toString());
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/error.jsp");
			dispatcher.forward(request, response);
		}

			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/buyFi.jsp");
			dispatcher.forward(request, response);
        }
        }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
