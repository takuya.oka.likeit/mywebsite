package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemBeans;
import beans.UserBeans;
import dao.ItemDao;

/**
 * Servlet implementation class ItemManagementSearch
 */
@WebServlet("/ItemManagementSearch")
public class ItemManagementSearch extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public ItemManagementSearch() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		UserBeans user = (UserBeans)session.getAttribute("userLogin");
		if(user == null) {
			session.removeAttribute("cartItem");
		    response.sendRedirect("Login");
		}

		if(user != null) {
		String category = request.getParameter("category");
		String maker = request.getParameter("maker");
		String price1 = request.getParameter("UPrice");
		String price2 = request.getParameter("OPrice");
        String name = request.getParameter("ItemName");

		ItemDao itemDao = new ItemDao();
		List<ItemBeans> item;
		try {
			item = itemDao.itemSearch(category, maker, price1, price2, name);

		request.setAttribute("item",item);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			request.setAttribute("err",e.toString());
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/error.jsp");
			dispatcher.forward(request, response);
		}
		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/itemManagement.jsp");
		dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
