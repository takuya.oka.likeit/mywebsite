package controller;

import java.io.IOException;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.UserBeans;
import dao.UserDao;

/**
 * Servlet implementation class UserManagement
 */
@WebServlet("/UserManagement")
public class UserManagement extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserManagement() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession();
		UserBeans user = (UserBeans)session.getAttribute("userLogin");
		if(user == null) {
			session.removeAttribute("cartItem");
		    response.sendRedirect("Login");
		}

		if(user != null) {
		UserDao userDao = new UserDao();
		List<UserBeans> userAll;
		try {
			userAll = userDao.findUserAll();

		request.setAttribute("user", userAll);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			request.setAttribute("err",e.toString());
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/error.jsp");
			dispatcher.forward(request, response);
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userManagement.jsp");
		dispatcher.forward(request, response);
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		UserBeans user = (UserBeans)session.getAttribute("userLogin");
		if(user == null) {
			session.removeAttribute("cartItem");
		    response.sendRedirect("Login");
		}

		if(user != null) {
		String loginId = request.getParameter("loginId");
		String name = request.getParameter("name");
		String birthDate1 = request.getParameter("birthDate1");
		String birthDate2 = request.getParameter("birthDate2");

		UserDao userDao = new UserDao();
		List<UserBeans> userSearch;
		try {
			userSearch = userDao.userSearch(loginId, name, birthDate1, birthDate2);

		request.setAttribute("user", userSearch);

		} catch (SQLException e) {
			// TODO 自動生成された catch ブロック
			e.printStackTrace();
			request.setAttribute("err",e.toString());
			RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/error.jsp");
			dispatcher.forward(request, response);
		}

		RequestDispatcher dispatcher = request.getRequestDispatcher("WEB-INF/jsp/userManagement.jsp");
		dispatcher.forward(request, response);
		}
	}
}
