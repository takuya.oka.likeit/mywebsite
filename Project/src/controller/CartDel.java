package controller;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.ItemBeans;
import beans.UserBeans;

/**
 * Servlet implementation class CartDel
 */
@WebServlet("/CartDel")
public class CartDel extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public CartDel() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		HttpSession session = request.getSession();
		UserBeans user = (UserBeans)session.getAttribute("userLogin");
		if(user == null) {
		   session.removeAttribute("cartItem");
		   response.sendRedirect("Login");
		}

		if(user != null) {
		   String id =request.getParameter("id");

		   ArrayList<ItemBeans> cart = (ArrayList<ItemBeans>) session.getAttribute("cartItem");

		   int cartNumber = Integer.parseInt(id);

		   cart.remove(cartNumber);

		   response.sendRedirect("Cart");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

	}

}
