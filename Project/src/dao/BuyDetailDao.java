package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class BuyDetailDao {

  public void insertBuyDetail(int buyId,int itemId) throws SQLException{

	  Connection con = null;

	  try {
	    	 con = DBManager.getConnection();

	    	 String sql = "INSERT INTO buy_detail (buy_id,item_id) VALUES (?,?)";

	    	 PreparedStatement pst = con.prepareStatement(sql);
				pst.setInt(1,buyId);
				pst.setInt(2,itemId);

				pst.executeUpdate();

}catch (SQLException e) {
   e.printStackTrace();

} finally {

   if (con != null) {
       try {
           con.close();
       } catch (SQLException e) {
           e.printStackTrace();

       }
}
}
}
  }

