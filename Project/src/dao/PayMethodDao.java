package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.PayMethodBeans;

public class PayMethodDao {

	public List<PayMethodBeans> payMethodAll() throws SQLException{

		  Connection con = null;
		  try {
		    	 con = DBManager.getConnection();

		    	 List<PayMethodBeans> payList = new ArrayList<PayMethodBeans>();

		    	 String sql = "SELECT * FROM pay_method";

		    	  Statement stmt = con.createStatement();
				    ResultSet rs = stmt.executeQuery(sql);

				    while (rs.next()) {
				    	int idData = rs.getInt("id");
				    	String nameData = rs.getString("name");
				    	int priceData = rs.getInt("price");

				    	PayMethodBeans pay = new PayMethodBeans(idData,nameData,priceData);
				    	payList.add(pay);

				    }
		        return payList;
		  }catch (SQLException e) {
		        e.printStackTrace();
		        return null;
		    } finally {

		        if (con != null) {
		            try {
		                con.close();
		            } catch (SQLException e) {
		                e.printStackTrace();
		                return null;
		            }
		        }
		    }
	  }
	public PayMethodBeans payMethodSearch(String id) throws SQLException{

		  Connection con = null;
		  try {
		    	 con = DBManager.getConnection();

		    	 String sql = "SELECT * FROM pay_method WHERE id = ?";

		    	 PreparedStatement pst = con.prepareStatement(sql);
		    	 pst.setString(1,id);
				 ResultSet rs = pst.executeQuery();

				    if(!rs.next()) {
				    	return null;
				    }

				    	int idData = rs.getInt("id");
				    	String nameData = rs.getString("name");
				    	int priceData = rs.getInt("price");

				    	PayMethodBeans pay = new PayMethodBeans(idData,nameData,priceData);

		        return pay;
		  }catch (SQLException e) {
		        e.printStackTrace();
		        return null;
		    } finally {

		        if (con != null) {
		            try {
		                con.close();
		            } catch (SQLException e) {
		                e.printStackTrace();
		                return null;
		            }
		        }
		    }
	  }

}
