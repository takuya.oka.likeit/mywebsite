package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.BuyBeans;

public class BuyDao {

   public int buy(int userId,int totalPrice,int deliveryMethodId,int payMethodId) throws SQLException{

	  Connection con = null;

	  int id = -1;

	  try {
	    	 con = DBManager.getConnection();

	    	 String sql = "INSERT INTO buy (user_id,total_price,delivery_method_id,pay_method_id,buy_date) VALUES (?,?,?,?,now())";

	    	 PreparedStatement pst = con.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
				pst.setInt(1,userId);
				pst.setInt(2,totalPrice);
				pst.setInt(3,deliveryMethodId);
				pst.setInt(4,payMethodId);

				pst.executeUpdate();

				ResultSet rs = pst.getGeneratedKeys();
		if(rs.next()) {
				id = rs.getInt(1);
		}
				return id;
  }catch (SQLException e) {
      e.printStackTrace();
      return -1;
  } finally {

      if (con != null) {
          try {
              con.close();
          } catch (SQLException e) {
              e.printStackTrace();
              return-1;
          }
}
  }
}
   public List<BuyBeans> GetBuyHis() throws SQLException{
		Connection con = null;

		List<BuyBeans> buyHisList = new ArrayList<BuyBeans>();

		try {
	    	 con = DBManager.getConnection();

	    	 String sql ="SELECT"
	    	 		+ " buy.id AS b_id,buy.user_id,buy.buy_date,delivery_method.name AS d_name,pay_method.name AS p_name,buy.total_price"
	    	 		+ " FROM buy"
	    	 		+ " JOIN delivery_method ON"
	    	 		+ " buy.delivery_method_id = delivery_method.id"
	    	 		+ " JOIN pay_method"
	    	 		+ " ON buy.pay_method_id = pay_method.id";

	    	 PreparedStatement pst = con.prepareStatement(sql);

	    	 ResultSet rs = pst.executeQuery();

       while(rs.next()) {
    	   int buyId = rs.getInt("b_id");
    	   int userId = rs.getInt("user_id");
    	   String buyDate = rs.getString("buy_date");
    	   String dName = rs.getString("d_name");
    	   String pName = rs.getString("p_name");
    	   int totalPrice = rs.getInt("total_price");

    	   BuyBeans buy = new BuyBeans(buyId,userId,buyDate,dName,pName,totalPrice);
    	   buyHisList.add(buy);
       }

	    	 return buyHisList;

		}catch (SQLException e) {
		          e.printStackTrace();
		          return null;
		      } finally {
		          if (con != null) {
		              try {
		                  con.close();
		              } catch (SQLException e) {
		                  e.printStackTrace();
		                  return null;
		              }
		          }
		      }
}
   public BuyBeans GetBuyHisDetail(String buyId) throws SQLException{
		Connection con = null;

		try {
	    	 con = DBManager.getConnection();

	    	 String sql ="SELECT"
	    	 		+ " buy.user_id,buy.buy_date,delivery_method.name AS d_name,pay_method.name AS p_name,buy.total_price"
	    	 		+ " FROM buy"
	    	 		+ " JOIN delivery_method ON"
	    	 		+ " buy.delivery_method_id = delivery_method.id"
	    	 		+ " JOIN pay_method"
	    	 		+ " ON buy.pay_method_id = pay_method.id"
	    	 		+ " WHERE buy.id = ?";

	    	 PreparedStatement pst = con.prepareStatement(sql);
	    	 pst.setString(1,buyId);
	    	 ResultSet rs = pst.executeQuery();


	    	 if(!rs.next()) {
			    	return null;
			    }


   	   String buyDate = rs.getString("buy_date");
   	   String dName = rs.getString("d_name");
   	   String pName = rs.getString("p_name");
   	   int totalPrice = rs.getInt("total_price");

   	   BuyBeans buy = new BuyBeans(buyDate,dName,pName,totalPrice);

	    	 return buy;

		}catch (SQLException e) {
		          e.printStackTrace();
		          return null;
		      } finally {
		          if (con != null) {
		              try {
		                  con.close();
		              } catch (SQLException e) {
		                  e.printStackTrace();
		                  return null;
		              }
		          }
		      }
}
}
