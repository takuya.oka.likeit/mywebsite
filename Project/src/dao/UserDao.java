package dao;

import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.UserBeans;
import model.Encrypt;

public class UserDao {

	public UserBeans findLoginInfo(String loginId,String password) throws SQLException,NoSuchAlgorithmException{

     Connection con = null;
     try {
    	 con = DBManager.getConnection();

    	 String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

    	  PreparedStatement pst = con.prepareStatement(sql);
		    pst.setString(1, loginId);
		    pst.setString(2,Encrypt.cry(password));
		    ResultSet rs = pst.executeQuery();

		    if(!rs.next()) {
		    	return null;
		    }

            int idData = rs.getInt("id");
		    String nameData = rs.getString("name");
		    return new UserBeans(idData,nameData);
		}catch (SQLException | NoSuchAlgorithmException e) {
          e.printStackTrace();
          return null;
      } finally {

          if (con != null) {
              try {
                  con.close();
              } catch (SQLException e) {
                  e.printStackTrace();
                  return null;
              }
          }
      }
     }
	public List<UserBeans> findUserAll(String loginId) throws SQLException{

	     Connection con = null;
	     List<UserBeans> userList = new ArrayList<UserBeans>();
	     try {
	    	 con = DBManager.getConnection();

	    	 String sql = "SELECT * FROM user WHERE login_id = ?";

	    	 PreparedStatement pst = con.prepareStatement(sql);
			    pst.setString(1, loginId);
			    ResultSet rs = pst.executeQuery();

			    if(!rs.next()) {
			    	return null;
			    }

			    String loginIdData = rs.getString("login_id");

			    UserBeans userBeans = new UserBeans(loginIdData);
			    userList.add(userBeans);

			    return userList;

			}catch (SQLException e) {
	          e.printStackTrace();
	          return null;
	      } finally {
	          if (con != null) {
	              try {
	                  con.close();
	              } catch (SQLException e) {
	                  e.printStackTrace();
	                  return null;
	              }
	          }
	      }
	     }
	public List<UserBeans> findUserAll() throws SQLException{

	     Connection con = null;
	     List<UserBeans> userList = new ArrayList<UserBeans>();
	     try {
	    	 con = DBManager.getConnection();

	    	 String sql = "SELECT * FROM user WHERE login_id NOT IN('admin')";

	    	 Statement stmt = con.createStatement();
			    ResultSet rs = stmt.executeQuery(sql);

			    while (rs.next()) {
                int idData = rs.getInt("id");
			    String loginIdData = rs.getString("login_id");
			    String nameData = rs.getString("name");
			    String birthDateData = rs.getString("birth_date");
			    String createDateData = rs.getString("create_date");
			    String updateDateData = rs.getString("update_date");

			    UserBeans userBeans = new UserBeans(idData,loginIdData,nameData,birthDateData,createDateData,updateDateData);
			    userList.add(userBeans);
			    }
			    return userList;


			}catch (SQLException e) {
	          e.printStackTrace();
	          return null;
	      } finally {
	          if (con != null) {
	              try {
	                  con.close();
	              } catch (SQLException e) {
	                  e.printStackTrace();
	                  return null;
	              }
	          }
	      }
	     }
	public UserBeans findUserInfo(String id) throws SQLException{

	 Connection con = null;
	 try {
	     con = DBManager.getConnection();

	     String sql = "SELECT * FROM user WHERE id = ?";

	      PreparedStatement pst = con.prepareStatement(sql);
			pst.setString(1, id);
			ResultSet rs = pst.executeQuery();

		    if(!rs.next()) {
			    	return null;
			    }

		    int idData = rs.getInt("id");
		    String loginIdData = rs.getString("login_id");
		    String nameData = rs.getString("name");
		    String birthDateData = rs.getString("birth_date");
            String passwordData = rs.getString("password");
            String createDateData = rs.getString("create_date");
            String updateDate = rs.getString("update_date");

            return new UserBeans(idData,loginIdData,nameData,birthDateData,passwordData,createDateData,updateDate);

	}catch (SQLException e) {
        e.printStackTrace();
        return null;
    } finally {

        if (con != null) {
            try {
                con.close();
            } catch (SQLException e) {
                e.printStackTrace();
                return null;
            }
        }
    }
	}

	public void userUpd(String loginId,String password,String name,String birthDate) throws SQLException,NoSuchAlgorithmException{

		 Connection con = null;
		 try {
		     con = DBManager.getConnection();



		     if(!(password.isEmpty())){
		     String sql = "UPDATE user SET password = ?,name = ?,birth_date = ?,update_date = now() WHERE login_id = ? ";

		     PreparedStatement pst = con.prepareStatement(sql);
				pst.setString(1,Encrypt.cry(password));
				pst.setString(2,name);
				pst.setString(3,birthDate);
				pst.setString(4,loginId);
				pst.executeUpdate();
				}

		     if((password.isEmpty())){
		    	 String sql = "UPDATE user SET name = ?,birth_date = ?,update_date = now() WHERE login_id = ? ";

		    	 PreparedStatement pst = con.prepareStatement(sql);
		    	 pst.setString(1,name);
					pst.setString(2,birthDate);
					pst.setString(3,loginId);
					pst.executeUpdate();
		     }

		}catch (SQLException | NoSuchAlgorithmException e) {
	        e.printStackTrace();
	    } finally {

	        if (con != null) {
	            try {
	                con.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }
	    }
		}
	public void userRegi(String loginId,String name,String birthDate,String password) throws SQLException,NoSuchAlgorithmException{

		 Connection con = null;
		 try {
		     con = DBManager.getConnection();

		     String sql = "INSERT INTO user(login_id,name,birth_date,password,create_date,update_date) VALUES(?,?,?,?,now(),now()) ";

		     PreparedStatement pst = con.prepareStatement(sql);
				pst.setString(1,loginId);
				pst.setString(2,name);
				pst.setString(3,birthDate);
				pst.setString(4,Encrypt.cry(password));
				pst.executeUpdate();



		}catch (SQLException | NoSuchAlgorithmException e) {
	        e.printStackTrace();
	    } finally {

	        if (con != null) {
	            try {
	                con.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }
	    }
		}
	public void userDel(String id) throws SQLException{

		 Connection con = null;
		 try {
		     con = DBManager.getConnection();

		     String sql = "DELETE FROM user WHERE id = ? ";

		     PreparedStatement pst = con.prepareStatement(sql);
				pst.setString(1,id);
				pst.executeUpdate();


		}catch (SQLException e) {
	        e.printStackTrace();
	    } finally {

	        if (con != null) {
	            try {
	                con.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }
	    }
		}
	public List<UserBeans> userSearch(String loginId,String name,String birthDate1,String birthDate2) throws SQLException{
		 Connection con = null;
		 List<UserBeans> userList = new ArrayList<UserBeans>();
		 try {
		     con = DBManager.getConnection();

		     String sql = "SELECT * FROM user WHERE login_id NOT IN('admin')";

		     Statement stmt = con.createStatement();

		     if(!(loginId.isEmpty())) {
		    	 sql += "AND login_id = '" + loginId + "'";
		     }
		     if(!(name.isEmpty())) {
		    	 sql += "AND name LIKE '" + "%" + name  + "%" + "'";
		     }
		     if(!(birthDate1.isEmpty())) {
                 sql += "AND birth_date >= '" + birthDate1 + "'";
		     }
		     if(!(birthDate2.isEmpty())) {
		    	 sql += "AND birth_date <= '" + birthDate2 + "'";
		     }

		     ResultSet rs = stmt.executeQuery(sql);
		     while (rs.next()) {
		    	    int idData = rs.getInt("id");
	                String loginIdData = rs.getString("login_id");
	                String nameData = rs.getString("name");
	                String birthDate = rs.getString("birth_date");
	                String createDate = rs.getString("create_date");
	                String updateDate = rs.getString("update_date");
	                UserBeans user = new UserBeans(idData, loginIdData, nameData, birthDate, createDate, updateDate);

	                userList.add(user);
			}
				return userList;
		}catch (SQLException e) {
	        e.printStackTrace();
	        return null;
	    } finally {
	        if (con != null) {
	            try {
	                con.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                return null;
	            }
	        }
	    }

	}
	}

