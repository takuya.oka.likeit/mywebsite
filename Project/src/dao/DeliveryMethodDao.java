package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.DeliveryMethodBeans;

public class DeliveryMethodDao {

	public List<DeliveryMethodBeans> deliveryMethodAll() throws SQLException{

		  Connection con = null;
		  try {
		    	 con = DBManager.getConnection();

		    	 List<DeliveryMethodBeans> deliveryList = new ArrayList<DeliveryMethodBeans>();

		    	 String sql = "SELECT * FROM delivery_method";

		    	  Statement stmt = con.createStatement();
				    ResultSet rs = stmt.executeQuery(sql);

				    while (rs.next()) {
				    	int idData = rs.getInt("id");
				    	String nameData = rs.getString("name");
				    	int priceData = rs.getInt("price");

                        DeliveryMethodBeans delivery = new DeliveryMethodBeans(idData,nameData,priceData);
				    	deliveryList.add(delivery);

				    }
		        return deliveryList;
		  }catch (SQLException e) {
		        e.printStackTrace();
		        return null;
		    } finally {

		        if (con != null) {
		            try {
		                con.close();
		            } catch (SQLException e) {
		                e.printStackTrace();
		                return null;
		            }
		        }
		    }
	  }
	public DeliveryMethodBeans deliveryMethodSearch(String id) throws SQLException{

		  Connection con = null;
		  try {
		    	 con = DBManager.getConnection();

		    	 String sql = "SELECT * FROM delivery_method WHERE id = ?";

		    	 PreparedStatement pst = con.prepareStatement(sql);
		    	 pst.setString(1,id);
				 ResultSet rs = pst.executeQuery();

				    if(!rs.next()) {
				    	return null;
				    }

				    	int idData = rs.getInt("id");
				    	String nameData = rs.getString("name");
				    	int priceData = rs.getInt("price");

                      DeliveryMethodBeans delivery = new DeliveryMethodBeans(idData,nameData,priceData);

		        return delivery;
		  }catch (SQLException e) {
		        e.printStackTrace();
		        return null;
		    } finally {

		        if (con != null) {
		            try {
		                con.close();
		            } catch (SQLException e) {
		                e.printStackTrace();
		                return null;
		            }
		        }
		    }
	  }
}
