package dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import beans.ItemBeans;

public class ItemDao {

  public void addItem(String fileName,String category,String maker,String name,String price,String detail) throws SQLException{

	  Connection con = null;
	  try {
		     con = DBManager.getConnection();

		     String sql = "INSERT INTO item(detail,name,price,file_name,category,maker) VALUES(?,?,?,?,?,?) ";

		     PreparedStatement pst = con.prepareStatement(sql);
				pst.setString(1,detail);
				pst.setString(2,name);
				pst.setString(3,price);
				pst.setString(4,fileName);
				pst.setString(5, category);
				pst.setString(6, maker);

				pst.executeUpdate();

		}catch (SQLException e) {
	        e.printStackTrace();
	    } finally {

	        if (con != null) {
	            try {
	                con.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }
	    }
  }
  public List<ItemBeans> itemAll() throws SQLException{

	  Connection con = null;
	  try {
	    	 con = DBManager.getConnection();

	    	 List<ItemBeans> ItemList = new ArrayList<ItemBeans>();

	    	 String sql = "SELECT * FROM item";

	    	  Statement stmt = con.createStatement();
			    ResultSet rs = stmt.executeQuery(sql);

			    while (rs.next()) {
			    	int idData = rs.getInt("id");
			    	String fileNameData = rs.getString("file_name");
			    	String categoryData = rs.getString("category");
			    	String makerData = rs.getString("maker");
			    	String nameData = rs.getString("name");
			    	int priceData = rs.getInt("price");

			    	ItemBeans item = new ItemBeans(idData,fileNameData,categoryData,makerData,nameData,priceData);
			    	ItemList.add(item);

			    }
	        return ItemList;
	  }catch (SQLException e) {
	        e.printStackTrace();
	        return null;
	    } finally {

	        if (con != null) {
	            try {
	                con.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                return null;
	            }
	        }
	    }
  }
  public List<ItemBeans> itemSearch(String category,String maker,String price1,String price2,String name) throws SQLException{

	  Connection con = null;
	  try {
	    	 con = DBManager.getConnection();

	    	 List<ItemBeans> ItemList = new ArrayList<ItemBeans>();

	    	 String sql = "SELECT * FROM item WHERE id NOT IN(0)";

	    	  Statement stmt = con.createStatement();

	    	     if(!(category.isEmpty())) {
			    	 sql += "AND category = '" + category + "'";
			     }
			     if(!(maker.isEmpty())) {
			    	 sql += "AND maker LIKE '"+ "%" + maker + "%" + "'";
			     }
			     if(!(price1.isEmpty())) {
	                 sql += "AND price >= '" + price1 + "'";
			     }
			     if(!(price2.isEmpty())) {
			    	 sql += "AND price <= '" + price2 + "'";
			     }
			     if(!(name.isEmpty())) {
			    	 sql += "AND name LIKE '" + "%" + name + "%" + "'";
			     }

			    ResultSet rs = stmt.executeQuery(sql);

			    while (rs.next()) {
			    	int idData = rs.getInt("id");
			    	String fileNameData = rs.getString("file_name");
			    	String categoryData = rs.getString("category");
			    	String makerData = rs.getString("maker");
			    	String nameData = rs.getString("name");
			    	int priceData = rs.getInt("price");

			    	ItemBeans item = new ItemBeans(idData,fileNameData,categoryData,makerData,nameData,priceData);
			    	ItemList.add(item);

			    }
	        return ItemList;
	  }catch (SQLException e) {
	        e.printStackTrace();
	        return null;
	    } finally {

	        if (con != null) {
	            try {
	                con.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	                return null;
	            }
	        }
	    }
  }
  public void itemDel(String id) throws SQLException{

		 Connection con = null;
		 try {
		     con = DBManager.getConnection();

		     String sql = "DELETE FROM item WHERE id = ? ";

		     PreparedStatement pst = con.prepareStatement(sql);
				pst.setString(1,id);
				 pst.executeUpdate();


		}catch (SQLException e) {
	        e.printStackTrace();
	    } finally {

	        if (con != null) {
	            try {
	                con.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }
	    }
		}
  public ItemBeans itemSearch(String id) throws SQLException{

	  Connection con = null;
	  try {
	    	 con = DBManager.getConnection();

	    	 String sql = "SELECT * FROM item WHERE id = ?";

	    	 PreparedStatement pst = con.prepareStatement(sql);
				pst.setString(1,id);
				 ResultSet rs = pst.executeQuery();

				 if(!rs.next()) {
				    	return null;
				    }
				    int idData = rs.getInt("id");
				    String fileNameData = rs.getString("file_name");
			    	String categoryData = rs.getString("category");
			    	String makerData = rs.getString("maker");
			    	String nameData = rs.getString("name");
			    	int priceData = rs.getInt("price");
			    	String detailData = rs.getString("detail");

			    	ItemBeans item = new ItemBeans(idData,fileNameData,categoryData,makerData,nameData,priceData,detailData);

			    	return item;
  }catch (SQLException e) {
      e.printStackTrace();
      return null;
  } finally {

      if (con != null) {
          try {
              con.close();
          } catch (SQLException e) {
              e.printStackTrace();
              return null;
          }
}
  }
  }
  public void itemUpd(String id,String fileName,String category,String maker,String name,String price,String detail) throws SQLException{

		 Connection con = null;
		 try {
		     con = DBManager.getConnection();

		     String sql = "UPDATE item SET file_name = ?,category = ?,maker = ?,name = ?,price = ?,detail = ? WHERE id = ? ";

		     PreparedStatement pst = con.prepareStatement(sql);
				pst.setString(1,fileName);
				pst.setString(2,category);
				pst.setString(3,maker);
				pst.setString(4,name);
				pst.setString(5,price);
				pst.setString(6,detail);
				pst.setString(7,id);

				pst.executeUpdate();

		}catch (SQLException e) {
	        e.printStackTrace();
	    } finally {

	        if (con != null) {
	            try {
	                con.close();
	            } catch (SQLException e) {
	                e.printStackTrace();
	            }
	        }
	    }
		}
  public List<ItemBeans> GetBuyHisItem(String buyId) throws SQLException{
		Connection con = null;

		List<ItemBeans> buyHisItemList = new ArrayList<ItemBeans>();

		try {
	    	 con = DBManager.getConnection();

	    	 String sql ="SELECT"
	    	 		+ " item.category,item.maker,item.name,item.price"
	    	 		+ " FROM buy_detail"
	    	 		+ " JOIN item ON"
	    	 		+ " buy_detail.item_id = item.id"
	    	 		+ " WHERE buy_id = ?";

	    	 PreparedStatement pst = con.prepareStatement(sql);
	    	 pst.setString(1,buyId);
	    	 ResultSet rs = pst.executeQuery();

  while(rs.next()) {
  	String categoryData = rs.getString("category");
  	String makerData = rs.getString("maker");
  	String nameData = rs.getString("name");
  	int priceData = rs.getInt("price");

	   ItemBeans item = new ItemBeans(categoryData,makerData,nameData,priceData);
	   buyHisItemList.add(item);
  }

	    	 return buyHisItemList;

		}catch (SQLException e) {
		          e.printStackTrace();
		          return null;
		      } finally {
		          if (con != null) {
		              try {
		                  con.close();
		              } catch (SQLException e) {
		                  e.printStackTrace();
		                  return null;
		              }
		          }
		      }
}
}
