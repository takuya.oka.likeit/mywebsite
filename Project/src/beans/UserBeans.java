package beans;

import java.io.Serializable;

public class UserBeans implements Serializable {

	private int id;
	private String loginId;
    private String name;
    private String birthDate;
    private String password;
    private String createDate;
    private String updateDate;

    public UserBeans(String loginId,String password) {
    	this.loginId = loginId;
    	this.password = password;
    }

	public UserBeans(int idData, String nameData) {
		this.id = idData;
        this.name = nameData;
	}

	public UserBeans(int idData, String loginIdData, String nameData, String birthDateData, String passwordData, String createDateData, String updateDate) {
		this.id = idData;
		this.loginId = loginIdData;
		this.name = nameData;
		this.birthDate = birthDateData;
		this.password = passwordData;
		this.createDate = createDateData;
		this.updateDate = updateDate;
	}

	public UserBeans(String loginId, String name, String birthDate) {
		this.loginId = loginId;
		this.name = name;
		this.birthDate = birthDate;
	}

	public UserBeans(String loginIdData, String nameData,String birthDate,String passwordData) {
		this.loginId = loginIdData;
		this.name = nameData;
		this.birthDate = birthDate;
		this.password = passwordData;

	}

	public UserBeans(String loginIdData) {
		this.loginId = loginIdData;
	}

	public UserBeans(int idData, String loginIdData, String nameData, String birthDateData, String createDateData,
			String updateDateData) {
        this.id = idData;
        this.loginId = loginIdData;
        this.name = nameData;
        this.birthDate = birthDateData;
        this.createDate = createDateData;
        this.updateDate = updateDateData;
	}

	public UserBeans(String loginIdData, String nameData, String birthDateData, String createDateData, String updateDateData) {
		this.loginId = loginIdData;
        this.name = nameData;
        this.birthDate = birthDateData;
        this.createDate = createDateData;
        this.updateDate = updateDateData;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getLoginId() {
		return loginId;
	}

	public void setLoginId(String loginId) {
		this.loginId = loginId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getCreateDate() {
		return createDate;
	}

	public void setCreateDate(String createDate) {
		this.createDate = createDate;
	}

	public String getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(String updateDate) {
		this.updateDate = updateDate;
	}

}
