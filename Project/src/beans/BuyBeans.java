package beans;

import java.io.Serializable;

public class BuyBeans implements Serializable  {

  private int id;
  private int userId;
  private int totalPrice;
  private int deliveryMethodId;
  private int payMethodId;
  private String buyDate;
  private String deliveryMethodName;
  private int deliveryMethodPrice;
  private String payMethodName;
  private int payMethodPrice;

public BuyBeans(int buyId, int userId, String buyDate, String dName, String pName, int totalPrice) {
	this.id = buyId;
	this.userId = userId;
	this.buyDate = buyDate;
	this.deliveryMethodName = dName;
	this.payMethodName = pName;
	this.totalPrice = totalPrice;
}

public BuyBeans(String buyDate2, String dName, String pName, int totalPrice2) {
	this.buyDate = buyDate2;
	this.deliveryMethodName = dName;
	this.payMethodName = pName;
	this.totalPrice = totalPrice2;
}

public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public int getUserId() {
	return userId;
}
public void setUserId(int userId) {
	this.userId = userId;
}
public int getTotalPrice() {
	return totalPrice;
}
public void setTotalPrice(int totalPrice) {
	this.totalPrice = totalPrice;
}
public int getDeliveryMethodId() {
	return deliveryMethodId;
}
public void setDeliveryMethodId(int deliveryMethodId) {
	this.deliveryMethodId = deliveryMethodId;
}
public int getPayMethodId() {
	return payMethodId;
}
public void setPayMethodId(int payMethodId) {
	this.payMethodId = payMethodId;
}
public String getBuyDate() {
	return buyDate;
}
public void setBuyDate(String buyDate) {
	this.buyDate = buyDate;
}
public String getDeliveryMethodName() {
	return deliveryMethodName;
}
public void setDeliveryMethodName(String deliveryMethodName) {
	this.deliveryMethodName = deliveryMethodName;
}
public int getDeliveryMethodPrice() {
	return deliveryMethodPrice;
}
public void setDeliveryMethodPrice(int deliveryMethodPrice) {
	this.deliveryMethodPrice = deliveryMethodPrice;
}
public String getPayMethodName() {
	return payMethodName;
}
public void setPayMethodName(String payMethodName) {
	this.payMethodName = payMethodName;
}
public int getPayMethodPrice() {
	return payMethodPrice;
}
public void setPayMethodPrice(int payMethodPrice) {
	this.payMethodPrice = payMethodPrice;
}
}
