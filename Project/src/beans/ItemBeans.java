package beans;

import java.io.Serializable;

public class ItemBeans implements Serializable  {

  private int id;
  private String detail;
  private String name;
  private int price;
  private String fileName;
  private String category;
  private String maker;

public ItemBeans(int idData,String fileName, String categoryData, String makerData, String nameData,int priceData,String detail) {
	this.id = idData;
	this.fileName = fileName;
	this.category = categoryData;
	this.maker = makerData;
	this.name = nameData;
	this.price = priceData;
	this.detail = detail;
}
public ItemBeans(int idData, String categoryData, String makerData, String nameData,int priceData) {
	this.id = idData;
	this.category = categoryData;
	this.maker = makerData;
	this.name = nameData;
	this.price = priceData;
}
public ItemBeans(int idData, String fileNameData, String categoryData, String makerData, String nameData,
		int priceData) {
	this.id = idData;
	this.fileName = fileNameData;
	this.category = categoryData;
	this.maker = makerData;
	this.name = nameData;
	this.price = priceData;
}
public ItemBeans(String categoryData, String makerData, String nameData, int priceData) {
	this.category = categoryData;
	this.maker = makerData;
	this.name = nameData;
	this.price = priceData;
}
public int getId() {
	return id;
}
public void setId(int id) {
	this.id = id;
}
public String getDetail() {
	return detail;
}
public void setDetail(String detail) {
	this.detail = detail;
}
public String getName() {
	return name;
}
public void setName(String name) {
	this.name = name;
}
public int getPrice() {
	return price;
}
public void setPrice(int price) {
	this.price = price;
}
public String getFileName() {
	return fileName;
}
public void setFileName(String fileName) {
	this.fileName = fileName;
}
public String getCategory() {
	return category;
}
public void setCategory(String category) {
	this.category = category;
}
public String getMaker() {
	return maker;
}
public void setMaker(String maker) {
	this.maker = maker;
}
}
