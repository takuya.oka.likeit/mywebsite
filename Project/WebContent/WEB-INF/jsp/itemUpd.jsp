<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品情報更新</title>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
crossorigin="anonymous">
</head>
<body>
<div class="position-absolute top-0 end-0">
<table class="table table-borderless">
<tr>
<td>
いらっしゃいませ${userLogin.name}さん
</td>
</tr>
<tr>
<td align="right">
<a href="Logout" class="navbar-link logout-link">ログアウト</a>
    </td>
    </tr>
    </table>
    </div>
<div align="center">
<div class="p-3 mb-2 bg-secondary text-white">
<h1>商品情報更新</h1>
    </div>
<form action="ItemUpd" method="post" enctype ="multipart/form-data">
<table>
<tr>
<td>
<input type="hidden" name="id" value="${updItem.id}">
</td>
</tr>
<tr>
<th>
画像
</th>
<td>
<input type="file"  name="fileName" value="${updItem.fileName}" class="form-control">
</td>
</tr>
<tr>
<th>
カテゴリ
</th>
<td>
<input type="text"  name="category" value="${updItem.category}" class="form-control" placeholder="カテゴリ" required>
</td>
</tr>
<tr>
<th>
メーカー
</th>
<td>
<input type="text"  name="maker" value="${updItem.maker}" class="form-control" placeholder="メーカー" required>
</td>
</tr>
<tr>
<th>
商品名
</th>
<td>
<input type="text"  name="name" value="${updItem.name}" class="form-control" placeholder="商品名" required>
</td>
</tr>
<tr>
<th>
価格
</th>
<td>
<input type="text"  name="price" value="${updItem.price}" class="form-control" placeholder="価格" required>
</td>
</tr>
<tr>
<th>
詳細
</th>
<td>
<input type="text"  name="detail" value="${updItem.detail}" class="form-control" placeholder="詳細">
</td>
</tr>
<tr>
<th>
</th>
<td>
<button type="submit" class="btn btn-primary">更新</button>
<button type="button" class="btn btn-primary" onclick="location.href='ItemManagement'">キャンセル</button>
</td>
</tr>
    </table>
    </form>
    </div>
</body>
</html>