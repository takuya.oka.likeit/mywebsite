<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品詳細</title>
<link rel="stylesheet" href="css/list.css">
<link rel="stylesheet" href="css/position.css">
<link rel="stylesheet" href="css/imgSize.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
crossorigin="anonymous">
</head>
<body>
<header>
<div class="p-3 mb-2 bg-info text-white">
    <div align="center">
<h1 class="col-6">商品詳細</h1>
    </div>
<div class="position-absolute top-0 end-0">
<table class="table table-borderless">
<tr>
<td>
いらっしゃいませ${userLogin.name}さん
</td>
</tr>
<tr>
<td align="right">
<a href="Logout" class="navbar-link logout-link">ログアウト</a>
    </td>
    </tr>
    </table>
</div>
    </div>
    </header>
<div align="center">
<div class="col-3">
    <h1>${itemDetail.name}</h1>
    </div>
    <div class="card" style="width: 50rem;">
        <div align="center">
  <img src="img/${itemDetail.fileName}" class="sizeA">
        </div>
  <div class="card-body">
    <p class="card-text">${itemDetail.detail}</p>
    <a href="AddCart?id=${itemDetail.id}" class="btn btn-primary">カートに入れる</a>
    <c:if test="${userLogin.id == 1}">
    <a href="ItemDel?id=${itemDetail.id}" class="btn btn-primary">削除</a>
    <a href="ItemUpd?id=${itemDetail.id}" class="btn btn-primary">更新</a>
    </c:if>
  </div>
</div>
    </div>
    <div class="B">
    <div class="card" style="width: 15rem;">
        <div align="center">
  <img src="img/${itemDetail.fileName}" class="sizeA">
  <div class="card-body">
    <h5 class="card-title">${itemDetail.name}</h5>
            </div>
  </div>
  <ul class="list-group list-group-flush">
    <li class="list-group-item">${itemDetail.category}</li>
    <li class="list-group-item">${itemDetail.maker}</li>
    <li class="list-group-item">${itemDetail.price}円</li>
  </ul>
        </div>
    </div>
        <div class="P">
    <div  class="col-2">
    <ul class="cp_list">
    <li><a href="Index" class="card-link">トップ</a></li>
	<li> <a href="Cart" class="card-link">カート</a></li>
    <li><a href="UserInfo?id=${userLogin.id}" class="card-link">会員情報</a></li>
    <c:if test="${userLogin.id == 1}">
    <li><a href="Management" class="card-link">管理画面</a></li>
    </c:if>
</ul>
        </div>
        </div>
</body>
</html>