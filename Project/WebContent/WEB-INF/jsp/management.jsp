<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>管理画面</title>
<link rel="stylesheet" href="css/list.css">
<link rel="stylesheet" href="css/position.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
crossorigin="anonymous">
</head>
<body>
 <div class="U">
    <ul class="cp_list">
    <li><a href="Index" class="card-link">トップ</a></li>
	<li> <a href="Cart" class="card-link">カート</a></li>
    <li><a href="UserInfo?id=${userLogin.id}" class="card-link">会員情報</a></li>
</ul>
        </div>
<div class="position-absolute top-0 end-0">
<table class="table table-borderless">
<tr>
<td>
いらっしゃいませ${userLogin.name}さん
</td>
</tr>
<tr>
<td align="right">
<a href="Logout" class="navbar-link logout-link">ログアウト</a>
    </td>
    </tr>
    </table>
    </div>
<div align="center">
<div class="p-3 mb-2 bg-secondary text-white">
<h1>管理画面</h1>
    </div>
<button  type="button" class="btn btn-primary btn-lg" onclick="location.href='UserManagement'">ユーザー管理</button>
<button type="button" class="btn btn-secondary btn-lg" onclick="location.href='ItemManagement'">商品管理</button>
</div>
</body>
</html>