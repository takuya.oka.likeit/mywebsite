<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>会員登録（確認）</title>
<link rel="stylesheet" href="css/position.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
crossorigin="anonymous">
</head>
<body>
<div align="center">
    <div class="p-3 mb-2 bg-success text-white">
    <h1>会員登録（確認）</h1>
    </div>
    <form action="UserRegiCon" method="post">
<table border="0">
<tr>
<th>
ログインID
</th>
<td>
<input type="text"  name="loginId" value="${userRegi.loginId}" class="form-control" readonly>
</td>
</tr>
<tr>
<th>
パスワード
</th>
<td>
<input type="password"  name="password" value="${userRegi.password}" class="form-control" readonly>
</td>
</tr>
<tr>
<th>
パスワード（確認）
</th>
<td>
<input type="password"  name="passwordc" value="${userRegi.password}" class="form-control" readonly>
</td>
</tr>
<tr>
<th>
ユーザーネーム
</th>
<td>
<input type="text"  name="name" value="${userRegi.name}" class="form-control" readonly>
</td>
</tr>
<tr>
<th>
生年月日
</th>
<td>
<input type="text"  name="birthDate" value="${userRegi.birthDate}" class="form-control" readonly>
</td>
</tr>
<tr>
<th>
上記の内容で登録しますか？
</th>
<td>
<button type="submit" class="btn btn-primary">登録</button>
<button type="button" class="btn btn-primary" onclick="location.href='UserRegiInput'">キャンセル</button>
</td>
</tr>
    </table>
     </form>
     <form >

     </form>
</div>
</body>
</html>