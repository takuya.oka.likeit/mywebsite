<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>楽器マーケット</title>
<link rel="stylesheet" href="css/position.css">
<link rel="stylesheet" href="css/list.css">
<link rel="stylesheet" href="css/imgSize.css">
<link rel="stylesheet" href="css/character.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
crossorigin="anonymous">
</head>
<body>
<header>
<div class="p-3 mb-2 bg-info text-white">
<h1>楽器マーケット</h1>
当サイトは楽器の通信販売サイトです。
<div class="position-absolute top-0 end-0">
<table class="table table-borderless">
<tr>
<td>
いらっしゃいませ${userLogin.name}さん
</td>
</tr>
<tr>
<td align="right">
<a href="Logout" class="navbar-link logout-link">ログアウト</a>
    </td>
    </tr>
    </table>
</div>
    </div>
    </header>
    <div class="E">
<div class="card bg-light mb-3" style="max-width: 17rem;">
<form action="ItemSearch" method="get">
<table>
<tr>
<th>
カテゴリ
</th>
<td>
<input name="category" class="form-control">
</td>
</tr>
<tr>
<th>
メーカー
</th>
<td>
<input type="text"  name="maker" class="form-control">
</td>
</tr>
<tr>
<th>
価格下限
</th>
<td>
<input type="text" name="UPrice" list="Price" class="form-control" autocomplete="off">
<datalist id="Price">
<option value="3000">
<option value="5000">
<option value="10000">
<option value="30000">
<option value="50000">
<option value="100000">
<option value="200000">
</datalist>
</td>
</tr>
<tr>
<th>
価格上限
</th>
<td>
<input type="text" name="OPrice" list="Price" class="form-control" autocomplete="off">
</td>
</tr>
<tr>
<th>
商品名
</th>
<td>
<input type="text"  name="ItemName" class="form-control">
</td>
</tr>
<tr>
<th>
</th>
<td align="center">
<button type="submit" class="btn btn-primary btn-lg">検索</button>
</td>
</tr>
    </table>
    </form>
    </div>
    </div>
    <div class="F">
    <ul class="cp_list">
	<li> <a href="Cart" class="card-link">カート</a></li>
    <li><a href="UserInfo?id=${userLogin.id}" class="card-link">会員情報</a></li>
    <c:if test="${userLogin.id == 1}">
    <li><a href="Management" class="card-link">管理画面</a></li>
    </c:if>
</ul>
        </div>
        <div class="Z">
    <h2>商品一覧</h2>
    </div>
    <div class="M">
    <table>
    <tr>
    <c:forEach var="item" items="${itemAll}" varStatus="status">
    <td>
    <div class="card" style="width: 15rem;">
        <div align="center">
  <img src="img/${item.fileName}" class="sizeA">
  <div class="card-body">
    <h5 class="card-title">${item.name}</h5>
            </div>
</div>
  <ul class="list-group list-group-flush">
    <li class="list-group-item"><div align="center">${item.category}</div></li>
    <li class="list-group-item"><div align="center">${item.maker}</div></li>
    <li class="list-group-item"><div align="center">${item.price}円</div></li>
  </ul>

  <div class="card-body" align="center">
    <a href="ItemDetail?id=${item.id}" class="card-link">詳細</a>
    <a href="AddCart?id=${item.id}" class="card-link">カートに追加</a>
  </div>
</div>
    </td>
    <c:if test="${status.count % 5 == 0}">
    <tr>
    </tr>
    </c:if>
    </c:forEach>
        </tr>
        </table>
        </div>
</body>
</html>