<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>更新完了</title>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
crossorigin="anonymous">
</head>
<body>
<div align="center">
<div class="p-3 mb-2 bg-success text-white">
<h1>更新が完了しました</h1>
    </div>
    </div>
<div class="position-absolute top-50 start-50 translate-middle">
<form action="UserInfo" method="get">
<c:if test="${userLogin.id != 1}">
<input type="hidden" name="id" value="${userLogin.id}">
<button type="submit" class="btn btn-primary btn-lg">会員情報</button>
</c:if>
<c:if test="${userLogin.id == 1}">
<button type="button" class="btn btn-primary btn-lg" onclick="location.href='UserManagement'">ユーザー管理</button>
</c:if>
</form>
    </div>
</body>
</html>