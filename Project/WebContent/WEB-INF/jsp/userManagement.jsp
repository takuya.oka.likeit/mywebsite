<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー管理</title>
<link rel="stylesheet" href="css/list.css">
<link rel="stylesheet" href="css/position.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
crossorigin="anonymous">
</head>
<body>
<div class="V">
    <ul class="cp_list">
    <li><a href="Index" class="card-link">トップ</a></li>
	<li> <a href="Cart" class="card-link">カート</a></li>
    <li><a href="UserInfo?id=${userLogin.id}" class="card-link">会員情報</a></li>
     <li><a href="ItemManagement" class="card-link">商品管理</a></li>
</ul>
        </div>
<div class="position-absolute top-0 end-0">
<table class="table table-borderless">
<tr>
<td>
いらっしゃいませ${userLogin.name}さん
</td>
</tr>
<tr>
<td align="right">
<a href="Logout" class="navbar-link logout-link">ログアウト</a>
    </td>
    </tr>
    </table>
    </div>
<div align="center">
<div class="p-3 mb-2 bg-secondary text-white">
<h1>ユーザー管理</h1>
    </div>
    <form action="UserManagement" method="post">
<table>

<tr>
<th>
ログインID
</th>
<td>
<input type="text"  name="loginId" class="form-control" placeholder="ログインID">
</td>
</tr>
<tr>
<th>
ユーザーネーム
</th>
<td>
<input type="text"  name="name" class="form-control" placeholder="ユーザーネーム">
</td>
</tr>
<tr>
<th>
生年月日
</th>
<td>
<input type="date"  name="birthDate1" class="form-control" placeholder="生年月日">
</td>
</tr>
<tr>
<th>
</th>
<td>
<input type="date"  name="birthDate2" class="form-control" placeholder="生年月日">
</td>
</tr>
<tr>
<th>
</th>
<td align="center">
<button type="submit" class="btn btn-primary btn-lg">検索</button>
</td>
</tr>
    </table>
    </form>
    </div>
<div style="text-align:center">
<table class="table table-hover">
<thead class="table-secondary">
<tr>
<th>ログインID</th>
<th>ユーザーネーム</th>
<th>生年月日</th>
<th>登録日時</th>
<th>更新日時</th>
<th></th>
    </tr>
    </thead>
    <c:forEach var="user" items="${user}">
    <tr>
<td>${user.loginId}</td>
<td>${user.name}</td>
<td>${user.birthDate}</td>
<td>${user.createDate}</td>
<td>${user.updateDate}</td>
<td><a href="UserInfo?id=${user.id}"><button>参照</button></a>
    <a href="UserInfoUpd?loginId=${user.loginId}&name=${user.name}&birthDate=${user.birthDate}"><button>更新</button></a>
    <a href="UserDel?id=${user.id}&loginId=${user.loginId}&name=${user.name}&birthDate=${user.birthDate}&createDate=${user.createDate}&updateDate=${user.updateDate}"><button>削除</button></a></td>
</tr>
</c:forEach>
</table>
    </div>
    <div class="W">
    <button  type="button" class="btn btn-primary btn-lg" onclick="location.href='UserManagement'">全ユーザー</button>
    </div>
</body>
</html>