<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>カート</title>
<link rel="stylesheet" href="css/list.css">
<link rel="stylesheet" href="css/position.css">
<link rel="stylesheet" href="css/imgSize.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
crossorigin="anonymous">
</head>
<body>
<header>
<div class="position-absolute top-0 end-0">
<table class="table table-borderless">
<tr>
<td>
いらっしゃいませ${userLogin.name}さん
</td>
</tr>
<tr>
<td align="right">
<a href="Logout" class="navbar-link logout-link">ログアウト</a>
    </td>
    </tr>
    </table>
</div>
<div class="p-3 mb-2 bg-info text-white">
<h1 class="col-6">カート</h1>
    </div>
    </header>
    <div class="N">
    <ul class="cp_list">
    <li><a href="Index" class="card-link">トップ</a></li>
    <li><a href="UserInfo?id=${userLogin.id}" class="card-link">会員情報</a></li>
    <c:if test="${userLogin.id == 1}">
    <li><a href="Management" class="card-link">管理画面</a></li>
    </c:if>
    </ul>
        </div>
        <form action="Buy" method="get">
    <div  class="D">
  <div class="card" style="width: 12rem;">
<div class="card-header">
    <h3 align="center">カート内情報</h3>
  </div>
  <ul class="list-group list-group-flush">
    <li class="list-group-item"><h5 align="center">商品数</h5></li>
    <li class="list-group-item"><div align="right">${cartCount}点</div></li>
    <li class="list-group-item"><h5 align="center">合計金額</h5></li>
    <li class="list-group-item"><div align="right">${totalPrice}円</div></li>
     <li class="list-group-item"><div align="center"><button type="submit" class="btn btn-primary">購入</button></div></li>
  </ul>
</div>
        </div>
    <div  class="O">
    <table>
    <tr>
    <c:forEach var="cart" items="${cartItem}"  varStatus="status">
    <td>
    <div class="card" style="width: 15rem;">
        <div align="center">
  <img src="img/${cart.fileName}" class="sizeA">
  <div class="card-body">
    <h5 class="card-title">${cart.name}</h5>
            </div>
  </div>
  <ul class="list-group list-group-flush">
    <li class="list-group-item"><div align="center">${cart.category}</div></li>
    <li class="list-group-item"><div align="center">${cart.maker}</div></li>
    <li class="list-group-item"><div align="center">${cart.price}円</div></li>
  </ul>
  <div class="card-body" align="center">
    <a href="ItemDetail?id=${cart.id}" class="card-link">詳細</a>
    <a href="CartDel?id=${status.index}" class="card-link">削除</a>
  </div>
</div>
    </td>
    <c:if test="${status.count % 4 == 0}">
    <tr>
    </tr>
    </c:if>
    </c:forEach>
        </tr>
        </table>
    </div>
    </form>
    <div class="X">
    <button type="button" class="btn btn-primary" onclick="location.href='CartAllDell'">カート内クリア</button>
    </div>
</body>
</html>