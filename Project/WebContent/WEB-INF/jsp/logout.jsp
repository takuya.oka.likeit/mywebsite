<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ログアウト</title>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
crossorigin="anonymous">
</head>
<body>
<div class="position-absolute top-50 start-50 translate-middle">
<div class="card bg-light mb-3" style="width: 30rem;">
  <div class="card-header" align="center"><h1>ログアウト</h1></div>
  <div class="card-body">
      <div align="center">
    <h5 class="card-title">ログアウトしました。</h5>
          </div>
      <div align="center"><a href="Login">ログイン画面へ戻る</a></div>
  </div>
</div>
</div>
</body>
</html>