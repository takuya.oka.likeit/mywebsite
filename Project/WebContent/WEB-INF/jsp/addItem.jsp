<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品追加</title>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
crossorigin="anonymous">
</head>
<body>

<div align="center">
<div class="p-3 mb-2 bg-secondary text-white">
    <div class="col-3">
<h1>商品追加</h1>
    </div>
    <div class="position-absolute top-0 end-0">
<table class="table table-borderless">
<tr>
<td>
いらっしゃいませ${userLogin.name}さん
</td>
</tr>
<tr>
<td align="right">
<a href="Logout" class="navbar-link logout-link">ログアウト</a>
    </td>
    </tr>
    </table>
    </div>
    </div>
    </div>
       <div align="center">
 <div class="card bg-light mb-3" style="max-width: 30rem;">
 <form action="AddItem" method="post"  enctype ="multipart/form-data">
     <table>
        <tr>
<th>
画像
</th>
<td>
<input type="file"  name="file" class="form-control">
</td>
</tr>
     <tr>
<th>
カテゴリ
</th>
<td>
<input type="text" name="category" class="form-control" required>
</td>
</tr>
<tr>
<th>
メーカー
</th>
<td>
<input type="text"  name="maker" class="form-control" required>
</td>
</tr>
<tr>
<th>
商品名
</th>
<td>
<input type="text"  name="ItemName" class="form-control" required>
</td>
</tr>
<tr>
<th>
価格
</th>
<td>
<input type="text"  name="price" class="form-control" required>
</td>
</tr>
<tr>
<th>
商品詳細
</th>
<td>
<input type="text"  name="ItemDetail" class="form-control">
</td>
</tr>
<tr>
<th>
</th>
<td>
<button type="submit" class="btn btn-primary btn-lg">登録</button>
<button type="button" class="btn btn-primary btn-lg" onclick="location.href='ItemManagement'">キャンセル</button>
</td>
</tr>
    </table>
    </form>
     </div>
           </div>
</body>
</html>