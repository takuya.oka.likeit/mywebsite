<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
    <title>ログイン</title>
	<meta charset="UTF-8">
	<link rel="stylesheet" href="css/position.css">
    <link rel="stylesheet" href="css/login.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    </head>
<body>
    <c:if test="${errMes != null}">
          <div align="center">
          <font color = "Red">
           ${errMes}
           </font>
           </div>
           </c:if>
            <div class="form-wrapper">
  <h1>ログイン</h1>
  <form action="Login" method="post">
    <div class="form-item">
      <input type="text" name="loginId" required placeholder="ログインID" autofocus>
    </div>
    <div class="form-item">
      <input type="password" name="password" required placeholder="Password">
    </div>
    <div class="button-panel">
      <input type="submit" class="button" value="ログイン">
    </div>
  </form>
  <div class="form-footer">
    <p><a href="UserRegiInput">新規登録</a></p>
  </div>
</div>
	</body>
</html>
