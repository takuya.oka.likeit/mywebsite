<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入入力</title>
<link rel="stylesheet" href="css/form.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
crossorigin="anonymous">
</head>
<body>
<header>
<div class="p-3 mb-2 bg-info text-white">
    <div align="center">
<h1 class="col-6">購入入力</h1>
    </div>
<div class="position-absolute top-0 end-0">
<table class="table table-borderless">
<tr>
<td>
いらっしゃいませ${userLogin.name}さん
</td>
</tr>
<tr>
<td align="right">
<a href="Logout" class="navbar-link logout-link">ログアウト</a>
    </td>
    </tr>
    </table>
</div>
    </div>
    </header>
    <form action="BuyCon" method="get">
    <div align="center">
 <div class="card bg-light mb-3" style="max-width: 20rem;">

<table>
<tr>
<th>
配送方法
</th>
<td>
<select name="delivery">
<c:forEach var="delivery" items="${deliveryAll}">
<option value="${delivery.id}">${delivery.name}</option>
</c:forEach>
</select>
</td>
</tr>
<tr>
<th>
支払方法
</th>
<td>
<select name="pay">
<c:forEach var="pay" items="${payAll}">
<option value="${pay.id}">${pay.name}</option>
</c:forEach>
</select>
</td>
</tr>
<tr>
<td>
<div align="center">
<button type="submit" class="btn btn-primary btn-lg">購入</button>
<button type="button" class="btn btn-primary btn-lg" onclick="location.href='Cart'">キャンセル</button>
</div>
</td>
</tr>
    </table>
    </div>
        </div>
        </form>
</body>
</html>