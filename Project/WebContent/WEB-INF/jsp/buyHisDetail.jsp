<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入履歴詳細</title>
<link rel="stylesheet" href="css/list.css">
<link rel="stylesheet" href="css/position.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
crossorigin="anonymous">
</head>
<body>
<header>
<div class="p-3 mb-2 bg-info text-white">
    <div align="center">
<h1 class="col-6">詳細</h1>
    </div>
<div class="position-absolute top-0 end-0">
<table class="table table-borderless">
<tr>
<td>
いらっしゃいませ${userLogin.name}さん
</td>
</tr>
<tr>
<td align="right">
<a href="Logout" class="navbar-link logout-link">ログアウト</a>
    </td>
    </tr>
    </table>
</div>
    </div>
    </header>
      <div class="T">
    <div  class="col-2">
    <ul class="cp_list">
    <li><a href="Index" class="card-link">トップ</a></li>
	<li> <a href="Cart" class="card-link">カート</a></li>
    <li><a href="UserInfo?id=${userLogin.id}" class="card-link">会員情報</a></li>
    <c:if test="${userLogin.id == 1}">
    <li><a href="Management" class="card-link">管理画面</a></li>
    </c:if>
</ul>
        </div>
        </div>
    <div class="H">
<div style="text-align:center">
<div class="card bg-light mb-3" style="max-width: 60rem;">

    <table class="table">
    <thead>
    <tr>
    <th>
    購入日時
    </th>
        <th>
        支払方法
        </th>
        <th>
        配送方法
        </th>
        <th>
        合計金額
        </th>
    </tr>
        </thead>
    <tr>
    <td>
    ${buy.buyDate}
    </td>
    <td>
    ${buy.payMethodName}
    </td>
        <td>
        ${buy.deliveryMethodName}
        </td>
    <td>
    ${buy.totalPrice}円
    </td>
    </tr>
    <tr>
    <td></td>
    <td></td>
    <td></td>
    <td></td>
    </tr>
        <thead>
    <tr>
    <th>
    カテゴリ
    </th>
    <th>
    メーカー
    </th>
    <th>
    商品名
    </th>
    <th>
    価格
    </th>
    </tr>
        </thead>
        <c:forEach var="item" items="${itemList}">
    <tr>
    <td>
    ${item.category}
    </td>
        <td>
        ${item.maker}
        </td>
        <td>
        ${item.name}
        </td>
        <td>
        ${item.price}円
        </td>
        </tr>
        </c:forEach>
    </table>
    </div>
    </div>
</div>
</body>
</html>