<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>購入確認</title>
<link rel="stylesheet" href="css/position.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
crossorigin="anonymous">
</head>
<body>
<header>
<div class="p-3 mb-2 bg-info text-white">
    <div align="center">
<h1 class="col-6">購入確認</h1>
    </div>
<div class="position-absolute top-0 end-0">
<table class="table table-borderless">
<tr>
<td>
いらっしゃいませ${userLogin.name}さん
</td>
</tr>
<tr>
<td align="right">
<a href="Logout" class="navbar-link logout-link">ログアウト</a>
    </td>
    </tr>
    </table>
</div>
    </div>
    </header>
    <form action="BuyFi" method="get">
    <div align="center">
    <h2>この内容で購入しますか？</h2>
        <button type="submit" class="btn btn-primary btn-lg">購入</button>
        <button type="button" class="btn btn-primary btn-lg" onclick="location.href='Cart'">キャンセル</button>
    </div>
    <div class="G">
    <div style="text-align:center">
<table class="table">
<thead class="table-secondary">
<tr>
<th>カテゴリ</th>
<th>メーカー</th>
<th>商品名</th>
<th>価格</th>
    </tr>
    </thead>
    <c:forEach var="cart" items="${cartItem}">
    <tr>
<td>${cart.category}</td>
<td>${cart.maker}</td>
<td>${cart.name}</td>
<td>${cart.price}円</td>
</tr>
</c:forEach>
</table>
        </div>
    </div>
    <div class="I">
    <div class="card bg-light mb-3" style="max-width: 40rem;">
<table>
<tr>
<th>
配送方法
</th>
<td>
<input type="hidden" name="DeliveryId" value="${delivery.id}">
<input type="text" name="DeliveryName" value="${delivery.name}"  class="form-control" readonly>
</td>
</tr>
<tr>
<th>
支払方法
</th>
<td>
<input type="hidden" name="PayId" value="${pay.id}">
<input type="text" name="PayMethodName" value="${pay.name}" class="form-control" readonly>
</td>
</tr>
    </table>
        </div>
    </div>
    <input type="hidden" name="totalPrice" value="${totalPrice}">
   </form>
   <div class="Q">
   <div class="card" style="width: 18rem;">
  <ul class="list-group list-group-flush">
    <li class="list-group-item"><div align="center">合計金額</div></li>
    <li class="list-group-item"><div align="center">${totalPrice}円</div></li>
  </ul>
  </div>
</div>
<div class="R">
<div style="text-align:center">
<table class="table table-bordered">
<thead>
    <tr>
      <th scope="col">手数料</th>
      <th scope="col">金額</th>
      </tr>
      </thead>
  <tbody>
    <tr>
      <th scope="row">${delivery.name}</th>
      <td>${delivery.price}円</td>
    </tr>
    <tr>
      <th scope="row">${pay.name}</th>
      <td>${pay.price}円</td>
    </tr>
  </tbody>
</table>
</div>
</div>
</body>
</html>