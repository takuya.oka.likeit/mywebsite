<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>会員情報</title>
<link rel="stylesheet" href="css/position.css">
<link rel="stylesheet" href="css/list.css">
<link rel="stylesheet" href="css/form.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
crossorigin="anonymous">
</head>
<body>
<div class="position-absolute top-0 end-0">
<table class="table table-borderless">
<tr>
<td>
いらっしゃいませ${userLogin.name}さん
</td>
</tr>
<tr>
<td align="right">
<a href="Logout" class="navbar-link logout-link">ログアウト</a>
    </td>
    </tr>
    </table>
    </div>
<div class="S">
    <ul class="cp_list">
    <li><a href="Index" class="card-link">トップ</a></li>
	<li> <a href="Cart" class="card-link">カート</a></li>
    <c:if test="${userLogin.id == 1}">
    <li><a href="Management" class="card-link">管理画面</a></li>
    </c:if>
</ul>
        </div>
<div align="center">
<div class="p-3 mb-2 bg-success text-white">
<h1>会員情報</h1>
    </div>

<form action="UserInfoUpd" method="get">
<table>
<tr>
<th>
ログインID　 ：
</th>
<td>
<input type="text" value="${userInfo.loginId}" name="loginId" class="formA" readonly>
</td>
</tr>
<tr>
<th>
ユーザーネーム：
</th>
<td>
<input type="text" value="${userInfo.name}" name="name" class="formA" readonly>
</td>
</tr>
<tr>
<th>
生年月日　 ：
</th>
<td>
<input type="date" value="${userInfo.birthDate}" name="birthDate" class="formA" readonly>
</td>
</tr>
<tr>
<th>
登録日時　 ：
</th>
<td>
<input type="text" value="${userInfo.createDate}"  class="formA" readonly>
</td>
</tr>
<tr>
<th>
更新日時　 ：
</th>
<td><input type="text" value="${userInfo.updateDate}" class="formA" readonly>
</td>
</tr>
<tr>
<td>
<div style="margin-left:auto; width:170px;">
 <button type="submit" class="btn btn-primary">会員情報を更新する</button>
</div>
</td>
</tr>
    </table>
    </form>
<br>
<br>
<h2>購入履歴</h2>
<div style="text-align:center">
<table class="table table-hover">
<thead>
<tr>
<th>購入日時</th>
<th>支払方法</th>
<th>配送方法</th>
<th>合計金額</th>
<th></th>
</tr>
</thead>
<c:forEach var="buy" items="${buyHis}">
<c:if test="${userLogin.id == buy.userId}">
<tr>
<td>${buy.buyDate}</td>
<td>${buy.payMethodName}</td>
<td>${buy.deliveryMethodName}</td>
<td>${buy.totalPrice}円</td>
<td><a href="BuyHisDetail?id=${buy.id}"><button>詳細</button></a></td>
</tr>
</c:if>
</c:forEach>
</table>
</div>
    </div>
</body>
</html>