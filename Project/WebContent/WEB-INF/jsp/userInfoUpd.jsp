<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>会員情報更新</title>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
crossorigin="anonymous">
</head>
<body>
<div align="center">
<div class="p-3 mb-2 bg-success text-white">
<h1>会員情報更新</h1>
    </div>
    <c:if test="${errUpd != null}">
    ${errUpd}
    </c:if>
<form action="UserInfoUpd" method="post">
<table>
<tr>
<th>
ログインID
</th>
<td>
<input type="text"  name="loginId" class="form-control" value="${userUpd.loginId}" readonly>
</td>
</tr>
<tr>
<th>
パスワード
</th>
<td>
<input type="password"  name="password" class="form-control"  placeholder="パスワード">
</td>
</tr>
<tr>
<th>
パスワード（確認）
</th>
<td>
<input type="password"  name="passwordc" class="form-control" placeholder="パスワード">
</td>
</tr>
<tr>
<th>
ユーザーネーム
</th>
<td>
<input type="text"  name="name" class="form-control" value="${userUpd.name}" placeholder="ユーザーネーム" required>
</td>
</tr>
<tr>
<th>
生年月日
</th>
<td>
<input type="date"  name="birthDate" class="form-control" value="${userUpd.birthDate}" placeholder="生年月日" required>
</td>
</tr>
<tr>
<th>
</th>
<td>
<button type="submit" class="btn btn-primary">更新</button>
</td>
</tr>
    </table>
    </form>
    </div>
</body>
</html>