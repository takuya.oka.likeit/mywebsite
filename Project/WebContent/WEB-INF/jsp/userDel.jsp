<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー削除</title>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
crossorigin="anonymous">
</head>
<body>
<div class="position-absolute top-0 end-0">
<table class="table table-borderless">
<tr>
<td>
いらっしゃいませ${userLogin.name}さん
</td>
</tr>
<tr>
<td align="right">
<a href="Logout" class="navbar-link logout-link">ログアウト</a>
    </td>
    </tr>
    </table>
    </div>
<div align="center">
<div class="p-3 mb-2 bg-secondary text-white">
<h1>ユーザー削除</h1>
    </div>
        <div class="card bg-light mb-3" style="width: 30rem;">
        <form action="UserDel" method="post">
<table>
<tr>
<td>
<input type="hidden"  name="id" value="${userDelId}" class="form-control">
</td>
</tr>
<tr>
<th>
ログインID
</th>
<td>
<input type="text"  name="loginId" value="${userDel.loginId}" class="form-control" readonly>
</td>
</tr>
<tr>
<th>
ユーザーネーム
</th>
<td>
<input type="text"  name="name" value="${userDel.name}" class="form-control" readonly>
</td>
</tr>
<tr>
<th>
生年月日
</th>
<td>
<input type="date"  name="birthDate" value="${userDel.birthDate}" class="form-control" readonly>
</td>
</tr>
<tr>
<th>
登録日時
</th>
<td>
<input type="date"  name="birthDate" value="${userDel.createDate}" class="form-control" readonly>
</td>
</tr>
<tr>
<th>
更新日時
</th>
<td>
<input type="date"  name="birthDate" value="${userDel.updateDate}" class="form-control"  readonly>
</td>
</tr>
<tr>
<th>
上記ユーザーを削除しますか？
</th>
<td>
<button type="submit" class="btn btn-primary">削除</button>
<button type="button" class="btn btn-primary" onclick="location.href='UserManagement'">キャンセル</button>
</td>
</tr>
    </table>
    </form>
        </div>
</div>
</body>
</html>