<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品削除</title>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
crossorigin="anonymous">
</head>
<body>
<div align="center">
<div class="p-3 mb-2 bg-secondary text-white">
<h1>商品削除</h1>
<div class="position-absolute top-0 end-0">
<table class="table table-borderless">
<tr>
<td>
いらっしゃいませ${userLogin.name}さん
</td>
</tr>
<tr>
<td align="right">
<a href="Logout" class="navbar-link logout-link">ログアウト</a>
    </td>
    </tr>
    </table>
    </div>
    </div>
    </div>
    <div align="center">
        <div class="card bg-light mb-3" style="width: 30rem;">
        <form action="ItemDel" method="post">
<table>
<tr>
<th>
商品ID
</th>
<td>
<input type="text"  name="itemId" value="${delItem.id}" class="form-control" readonly>
</td>
</tr>
<tr>
<th>
カテゴリー
</th>
<td>
<input type="text"  name="category" value="${delItem.category}" class="form-control" readonly>
</td>
</tr>
<tr>
<th>
メーカー
</th>
<td>
<input type="text"  name="maker" value="${delItem.maker}" class="form-control" readonly>
</td>
</tr>
<tr>
<th>
商品名
</th>
<td>
<input type="text"  name="itemName" value="${delItem.name}" class="form-control" readonly>
</td>
</tr>
<tr>
<th>
価格
</th>
<td>
<input type="text"  name="price" value="${delItem.price}" class="form-control" readonly>
</td>
</tr>
<tr>
<th>
上記商品を削除しますか？
</th>
<td>
<button type="submit" class="btn btn-primary">削除</button>
<button type="button" class="btn btn-primary" onclick="location.href='ItemManagement'">キャンセル</button>
</td>
</tr>
    </table>
    </form>
    </div>
        </div>
</body>
</html>