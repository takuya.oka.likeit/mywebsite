<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>エラー</title>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
crossorigin="anonymous">
</head>
<body>
<div class="position-absolute top-50 start-50 translate-middle">
<div class="card text-white bg-danger mb-3">
  <div class="card-header" align="center"><h1>システムエラー</h1></div>
  <div class="card-body">
    <div align="center"><h5 class="card-title">システムエラーが発生しました。</h5></div>
    <p class="card-text">エラーコード：${err}</p>
      <div align="center"><button class="btn btn-primary" onclick="location.href='Index'">トップへ戻る</button></div>
  </div>
</div>
</div>
</body>
</html>