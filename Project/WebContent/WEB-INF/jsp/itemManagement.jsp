<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>商品管理</title>
<link rel="stylesheet" href="css/list.css">
<link rel="stylesheet" href="css/position.css">
<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css"
rel="stylesheet"
integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1"
crossorigin="anonymous">
</head>
<body>
<div class="position-absolute top-0 end-0">
<table class="table table-borderless">
<tr>
<td>
いらっしゃいませ${userLogin.name}さん
</td>
</tr>
<tr>
<td align="right">
<a href="Logout" class="navbar-link logout-link">ログアウト</a>
    </td>
    </tr>
    </table>
    </div>
<div align="center">
<div class="p-3 mb-2 bg-secondary text-white">
<h1>商品管理</h1>
</div>
<div class="K">
    <button type="button" onclick="location.href='AddItem'">商品追加</button>
    </div>
    <form action="ItemManagementSearch" method="get">
<table>
<tr>
<th>
カテゴリ
</th>
<td>
<input type="text"  name="category" class="form-control">
</td>
</tr>
<tr>
<th>
メーカー
</th>
<td>
<input type="text"  name="maker" class="form-control">
</td>
</tr>
<tr>
<th>
価格下限
</th>
<td>
<input type="text" name="UPrice" list="Price" class="form-control" autocomplete="off">
<datalist id="Price">
<option value="3000">
<option value="5000">
<option value="10000">
<option value="30000">
<option value="50000">
<option value="100000">
<option value="200000">
</datalist>
</td>
</tr>
<tr>
<th>
価格上限
</th>
<td>
<input type="text" name="OPrice" list="Price" class="form-control" autocomplete="off">
</td>
</tr>
<tr>
<th>
商品名
</th>
<td>
<input type="text"  name="ItemName" class="form-control">
</td>
</tr>
<tr>
<th>
</th>
<td align="center">
<button type="submit" class="btn btn-primary btn-lg">検索</button>
</td>
</tr>
    </table>
    </form>
<div style="text-align:center">
<table class="table table-hover">
<thead class="table-secondary">
<tr>
<th>商品ID</th>
<th>カテゴリ</th>
<th>メーカー</th>
<th>商品名</th>
<th>価格</th>
<th></th>
    </tr>
    </thead>
    <c:forEach var="item" items="${item}">
    <tr>
<td>${item.id}</td>
<td>${item.category}</td>
<td>${item.maker}</td>
<td>${item.name}</td>
<td>${item.price}</td>

<td><a href="ItemDetail?id=${item.id}"><button>参照</button></a>
    <a href="ItemUpd?id=${item.id}"><button>更新</button></a>
    <a href="ItemDel?id=${item.id}"><button>削除</button></a></td>
</tr>
</c:forEach>
</table>
    </div>
    </div>
    <div class="L">
    <ul class="cp_list">
    <li><a href="Index" class="card-link">トップ</a></li>
	<li> <a href="Cart" class="card-link">カート</a></li>
    <li><a href="UserInfo?id=${userLogin.id}" class="card-link">会員情報</a></li>
    <li><a href="UserManagement" class="card-link">ユーザー管理</a></li>
</ul>
        </div>
<div class="Y">
        <button type="button" class="btn btn-primary btn-lg" onclick="location.href='ItemManagement'">全商品</button>
        </div>
</body>
</html>